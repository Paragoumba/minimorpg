package fr.paragoumba.minimorpg;

import fr.paragoumba.minimorpg.entities.Entity;

public class Collisions {

    private Collisions(){}

    public static boolean pointHitEntity(int x, int y, Entity entity){

        return x >= entity.getHitboxX() && x < entity.getHitboxX() + entity.getHitboxWidth() && 
                y >= entity.getHitboxY() && y < entity.getHitboxY() + entity.getHitboxHeight();

    }

    // fixme
    public static boolean entityHitEntity(Entity entity1, Entity entity2, int offsetX, int offsetY){

        int x1 = entity1.getHitboxX() + offsetX;
        int y1 = entity1.getHitboxY() + offsetY;
        int x2 = x1 + entity1.getHitboxWidth() + offsetX;
        int y2 = y1 + entity1.getHitboxHeight() + offsetY;

        return  pointHitEntity(x1, y1, entity2) ||
                pointHitEntity(x2, y1, entity2) ||
                pointHitEntity(x1, y2, entity2) ||
                pointHitEntity(x2, y2, entity2);

    }

    // fixme
    public static boolean entityHitEntityV2(Entity entity1, Entity entity2, int offsetX, int offsetY){

        int e1x1 = entity1.getHitboxX() + offsetX;
        int e1y1 = entity1.getHitboxY() + offsetY;
        int e1x2 = e1x1 + entity1.getHitboxWidth() + offsetX;
        int e1y2 = e1y1 + entity1.getHitboxHeight() + offsetY;

        int e2x1 = entity2.getHitboxX() + offsetX;
        int e2y1 = entity2.getHitboxY() + offsetY;
        int e2x2 = e2x1 + entity2.getHitboxWidth() + offsetX;
        int e2y2 = e2y1 + entity2.getHitboxHeight() + offsetY;

        return e1x1 < e2x2 && e1x2 > e2x1 && e1y1 < e2y2 && e1y2 > e2y1;

    }
}
