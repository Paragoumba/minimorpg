package fr.paragoumba.minimorpg.display.animations;

import fr.paragoumba.minimorpg.display.Frame;

import java.util.Collections;
import java.util.List;

public class StaticAnimation extends Animation {

    public StaticAnimation(String id, Frame frame) {

        super(id);

        this.frame = frame;

    }

    private Frame frame;

    @Override
    public String getActualFrameName(){

        return frame.getFrameId();

    }

    @Override
    public List<String> getAllFramesIds(){

        return Collections.singletonList(frame.getFrameId());

    }
}
