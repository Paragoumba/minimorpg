package fr.paragoumba.minimorpg.display.inventories;

import fr.paragoumba.minimorpg.ResourceManager;
import fr.paragoumba.minimorpg.display.Insets;
import fr.paragoumba.minimorpg.game.GameManager;

import java.awt.*;

public class Inventory extends InventoryComponent {

    public Inventory(){

        super();

        GameManager.game.getWindow().pack();
        GameManager.game.getWindow().setVisible(true);

        Dimension windowSize = GameManager.game.getWindow().getSize();
        java.awt.Insets windowInsets = GameManager.game.getWindow().getInsets();
        insets = new Insets(20, 0, 20, 0);

        // TODO get screen resolution
        width = 1600 * 2 / 3;
        height = 900 - 80 - windowInsets.top;

        relativeX = 1600 / 6;
        relativeY = 40;

    }

    public Inventory(String title){

        this();

        this.title = title;

    }

    private String title;

    @Override
    public void render(Graphics g) {

        // TODO finish render
        g.setColor(new Color(170, 168, 29));
        g.fillRect(0, 0, getWidth(), getHeight());

        if (title != null){

            g.setFont(ResourceManager.inventoryFont);
            g.setColor(Color.WHITE);
            g.drawString(title, 10, 10 + g.getFontMetrics().getAscent());
            //g.drawRect(10, 10, g.getFontMetrics().stringWidth(title), g.getFontMetrics().getAscent());

        }

        super.render(g);

    }
}
