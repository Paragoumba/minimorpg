package fr.paragoumba.minimorpg.display.inventories;

import fr.paragoumba.minimorpg.display.Component;

import java.awt.*;
import java.util.ArrayList;

public abstract class InventoryComponent extends Component {

    public InventoryComponent(){

        this(0, 0, 0, 0);

    }

    public InventoryComponent(int relativeX, int relativeY, int width, int height){

        super(relativeX, relativeY, width, height);

        childComponents = new ArrayList<>();

    }

    protected ArrayList<InventoryComponent> childComponents;

    @Override
    public void render(Graphics g){

        for (InventoryComponent childComponent : childComponents){

            childComponent.render(
                    g.create(
                            insets.left + childComponent.relativeX,
                            insets.top + childComponent.relativeY,
                            Math.min(width - insets.left - insets.right, childComponent.width),
                            Math.min(height - insets.top - insets.bottom, childComponent.height)
                    )
            );
        }
    }

    public void addComponent(InventoryComponent component){

        childComponents.add(component);

    }
}
