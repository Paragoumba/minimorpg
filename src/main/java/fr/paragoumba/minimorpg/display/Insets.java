package fr.paragoumba.minimorpg.display;

public class Insets {

    public Insets(int top, int right, int bottom, int left){

        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;

    }

    public int top, right, bottom, left;

}
