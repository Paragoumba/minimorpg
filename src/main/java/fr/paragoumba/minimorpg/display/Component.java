package fr.paragoumba.minimorpg.display;

import java.awt.*;
import java.util.ArrayList;

public abstract class Component {

    public Component(){

        this(0, 0, 0, 0);

    }

    public Component(int relativeX, int relativeY, int width, int height){

        this.relativeX = relativeX;
        this.relativeY = relativeY;
        this.width = width * resolution;
        this.height = height * resolution;
        childComponents = new ArrayList<>();
        insets = new Insets(0, 0, 0, 0);

    }

    protected final int resolution = 3;
    public int relativeX, relativeY, width, height;
    protected ArrayList<Component> childComponents;
    protected Insets insets;

    public void render(Graphics g){

        for (Component childComponent : childComponents){

            childComponent.render(
                    g.create(
                            insets.left + childComponent.relativeX,
                            insets.top + childComponent.relativeY,
                            Math.min(width - insets.left - insets.right, childComponent.width),
                            Math.min(height - insets.top - insets.bottom, childComponent.height)
                    )
            );
        }
    }

    public void addComponent(Component component){

        childComponents.add(component);

    }

    public int getWidth(){

        return width;

    }

    public int getHeight(){

        return height;

    }
}
