package fr.paragoumba.minimorpg.display;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.ParseResults;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import fr.paragoumba.minimorpg.commands.CommandSource;
import fr.paragoumba.minimorpg.entities.Entity;
import fr.paragoumba.minimorpg.entities.EntityHandler;
import fr.paragoumba.minimorpg.game.GameManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

import static com.mojang.brigadier.arguments.IntegerArgumentType.getInteger;
import static com.mojang.brigadier.arguments.IntegerArgumentType.integer;
import static com.mojang.brigadier.arguments.StringArgumentType.string;

public class Chat extends JComponent implements KeyListener {

    public Chat(){

        setPreferredSize(new Dimension(100, 20));

        dispatcher = new CommandDispatcher<>();

        dispatcher.register(
                literal("tp")
                        .then(
                                argument("x", integer())
                                        .then(
                                                argument("y", integer())
                                                        .executes(c -> {

                                                            ((Entity) c.getSource()).tp(getInteger(c, "x"), getInteger(c, "y"));
                                                            return 1;

                                                        })
                                        )
                                        .executes(c -> {

                                            System.out.println("Missing argument y.");
                                            return 1;

                                        })
                        )
                        /*.then(
                                argument("relativeX", string())
                                        .then(
                                                argument("relativeY", string())
                                                        .executes(c -> {

                                                            String relativeX = getString(c, "relativeX");
                                                            String relativeY = getString(c, "relativeY");

                                                            ((Entity) c.getSource())
                                                                    .move(
                                                                            relativeX.startsWith("~") ? Integer.parseInt(relativeX.substring(1)) : 0,
                                                                            relativeY.startsWith("~") ? Integer.parseInt(relativeY.substring(1)) : 0);
                                                            return 1;

                                                        })
                                        )
                                        .executes(c -> {

                                            System.out.println("Missing argument y.");
                                            return 1;

                                        })
                        )*/
                        .executes(c -> {

                            System.out.println("Too few arguments.");
                            return 1;

                        })
        );

        /*dispatcher.register(
                literal("help")
                        .then(
                                argument("command", string())
                                        .executes(c -> {
                                            System.out.println(dispatcher.);
                                            return 1;
                                        })
                        )
                        .executes(c -> {

                            for (dispatcher.getRoot().)

                            return 1;

                        })
        );*/
    }

    private static final Color backgroundColor = new Color(0, 0, 0, 200);

    private CommandDispatcher<CommandSource> dispatcher;

    private String actualMessage = "";
    private boolean isFocused = false;

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        if (isFocused) {

            //int textWidth = g.getFontMetrics().stringWidth(actualMessage);
            int textHeight = g.getFontMetrics().getHeight();

            g.setColor(backgroundColor);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.drawString(actualMessage, 5, (getHeight() + textHeight) / 2);

        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent){

        if (isFocused){

            if (keyEvent.getKeyChar() == '\b'){
                if (actualMessage.length() != 0){

                    actualMessage = actualMessage.substring(0, actualMessage.length() - 1);

                }

            } else if (keyEvent.getKeyChar() != '\n'){

                actualMessage += keyEvent.getKeyChar();

            }

            repaint();

        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent){}

    @Override
    public void keyReleased(KeyEvent keyEvent){

        if (isFocused){
            if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER){
                if (!actualMessage.equals("")){
                    if (actualMessage.charAt(0) == '/'){

                        ParseResults<CommandSource> parse = dispatcher.parse(
                                actualMessage.substring(1),
                                EntityHandler.getPlayer()
                        );

                        try {

                            int result = dispatcher.execute(parse);

                        } catch (CommandSyntaxException e){

                            e.printStackTrace();

                            System.out.println("#" + parse.getExceptions() + "#");

                        }

                    } else {

                        System.out.println(actualMessage);

                    }

                    actualMessage = "";

                }

                isFocused = false;

            }
        }
    }

    public boolean hasFocus(){

        return isFocused;

    }

    public void setFocused(boolean b){

        isFocused = b;

    }

    private static LiteralArgumentBuilder<CommandSource> literal(String name){

        return LiteralArgumentBuilder.literal(name);

    }

    private static <T> RequiredArgumentBuilder<CommandSource, T> argument(String name, ArgumentType<T> type){

        return RequiredArgumentBuilder.argument(name, type);

    }
}
