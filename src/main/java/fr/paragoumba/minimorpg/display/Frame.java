package fr.paragoumba.minimorpg.display;

public class Frame {

    public Frame(String frameId, Integer frameDuration) {

        this.frameId = frameId;
        this.frameDuration = frameDuration;

    }

    private String frameId;
    private Integer frameDuration;

    public String getFrameId() {

        return frameId;

    }

    public Integer getFrameDuration() {

        return frameDuration;

    }
}
