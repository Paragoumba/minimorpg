package fr.paragoumba.minimorpg.display;

import fr.paragoumba.minimorpg.KeyboardHandler;
import fr.paragoumba.minimorpg.MiniMORPG;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame {

    public Window(){

        this(MiniMORPG.APP_NAME + " - v" + MiniMORPG.VERSION);

    }

    public Window(String title){

       setPreferredSize(new Dimension(WIDTH, HEIGHT));
        //setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        mainPanel = new GamePanel();
        chat = new Chat();

        mainPanel.add(BorderLayout.SOUTH, chat);
        //mainPanel.setBackground(Color.BLACK);
        setContentPane(mainPanel);

        addKeyListener(new KeyboardHandler());
        addKeyListener(chat);

        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);

        pack();
        setVisible(true);

        System.out.println("w and h: " + getWidth()+" "+getHeight());

    }

    private int WIDTH = 800, HEIGHT = 600;

    private JPanel mainPanel;
    private Chat chat;

    public void render(){

        mainPanel.repaint();

    }

    public Chat getChat() {

        return chat;

    }
}
