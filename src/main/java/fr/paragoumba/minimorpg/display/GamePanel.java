package fr.paragoumba.minimorpg.display;

import fr.paragoumba.minimorpg.entities.Entity;
import fr.paragoumba.minimorpg.entities.EntityHandler;
import fr.paragoumba.minimorpg.entities.Player;
import fr.paragoumba.minimorpg.exceptions.AnimationNotFoundException;
import fr.paragoumba.minimorpg.exceptions.RenderException;

import javax.swing.*;
import java.awt.*;

public class GamePanel extends JPanel {

    public GamePanel(){

        super(new BorderLayout());

    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        try{

            render((Graphics2D) g);

        } catch (RenderException e) {

            e.printStackTrace();

        }
    }

    public void render(Graphics2D g2d) throws RenderException {

        for (Entity entity : EntityHandler.lockEntities()){

            entity.render(g2d);
            entity.drawHitbox(g2d);

        }

        EntityHandler.unlockEntities();

        for (Player player : EntityHandler.lockPlayers()){

            try {

                player.renderName(g2d);

            } catch (AnimationNotFoundException animationNotFoundException){

                animationNotFoundException.printStackTrace();

            }
        }

        EntityHandler.unlockPlayers();

    }
}
