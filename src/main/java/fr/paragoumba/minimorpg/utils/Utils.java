package fr.paragoumba.minimorpg.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Utils {

    private Utils(){}

    public static String getFileContent(URL url) throws IOException {

        URLConnection connection = url.openConnection();
        StringBuilder builder = new StringBuilder();

        try(InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader in = new BufferedReader(isr)){

            String line;

            while ((line = in.readLine()) != null){

                builder.append(line).append('\n');

            }
        }

        return builder.toString();

    }
}
