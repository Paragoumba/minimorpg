package fr.paragoumba.minimorpg.entities;

import fr.paragoumba.minimorpg.exceptions.RenderException;

import java.awt.*;

public class Character extends Entity {

    public Character(int x, int y, String name, EntityType type, String defaultAnimationId) {

        super(x, y, type, defaultAnimationId);

        this.name = name;

    }

    public static final String FRONT = "front";
    public static final String RIGHT = "right";
    public static final String LEFT = "left";
    public static final String BACK = "back";

    protected String name;

    public String getName() {

        return name;

    }

    @Override
    public void render(Graphics2D g2d) throws RenderException {

        g2d.drawImage(getActualTile(), x - getWidth() / 2, y - getHeight(), null);

    }

    @Override
    public int getHitboxX() {

        try{

            return x - getWidth() / 2;

        } catch (RenderException e) {

            e.printStackTrace();

        }

        return 0;

    }

    @Override
    public int getHitboxY() {

        return y - getHitboxHeight();

    }

    @Override
    public int getHitboxWidth() {

        try{

            return getWidth();

        } catch (RenderException e) {

            e.printStackTrace();

        }

        return 0;

    }

    @Override
    public int getHitboxHeight() {

        try{

            return getWidth() / 2;

        } catch (RenderException e) {

            e.printStackTrace();

        }

        return 0;

    }
}
