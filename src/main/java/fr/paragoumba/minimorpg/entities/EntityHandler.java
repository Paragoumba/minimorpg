package fr.paragoumba.minimorpg.entities;

import fr.paragoumba.minimorpg.display.inventories.Inventory;

import java.util.ArrayList;

public class EntityHandler {

    private static Player player;
    private static Inventory openedInventory;

    private static ArrayList<Entity> entities;
    private static ArrayList<Player> players;

    private static boolean entitiesLock = false;
    private static boolean playersLock = false;

    public static void init(){

        entities = new ArrayList<>();
        players = new ArrayList<>();

    }

    public static ArrayList<Entity> lockEntities(){

        while (entitiesLock){

            try {

                Thread.sleep(10);

            } catch (InterruptedException e){

                e.printStackTrace();

            }
        }

        entitiesLock = true;

        return entities;

    }

    public static ArrayList<Player> lockPlayers(){

        while (playersLock){

            try {

                Thread.sleep(10);

            } catch (InterruptedException e){

                e.printStackTrace();

            }
        }

        playersLock = true;

        return players;

    }

    public static void unlockEntities(){

        entitiesLock = false;

    }

    public static void unlockPlayers(){

        playersLock = false;

    }

    public static void addEntity(Entity entity){

        if (entity instanceof Player){

            players.add((Player) entity);

        }

        entities.add(entity);

    }

    public static Player getPlayer(){

        return player;

    }

    public static Inventory getOpenedInventory() {

        return openedInventory;

    }

    public static void setPlayer(Player player){

        EntityHandler.player = player;

    }

    public static void setOpenedInventory(Inventory openedInventory) {

        EntityHandler.openedInventory = openedInventory;

    }
}
