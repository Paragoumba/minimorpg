package fr.paragoumba.minimorpg.entities;

import fr.paragoumba.minimorpg.ResourceManager;
import fr.paragoumba.minimorpg.exceptions.RenderException;

import java.awt.*;

public class Player extends Character {

    public Player(int x, int y, String name, EntityType type, String defaultAnimationId) {

        super(x, y, name, type, defaultAnimationId);

    }

    public void renderName(Graphics2D g2d) throws RenderException {

        g2d.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setColor(Color.BLACK);
        g2d.setFont(ResourceManager.playerNameFont);

        int textWidth = g2d.getFontMetrics().stringWidth(name);
        int textHeight = g2d.getFontMetrics().getAscent();

        g2d.drawString(name,
                x - textWidth / 2,
                y - getHeight() - textHeight / 2);

    }
}
