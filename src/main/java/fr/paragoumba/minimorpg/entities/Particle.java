package fr.paragoumba.minimorpg.entities;

public abstract class Particle extends Entity {

    public Particle(int x, int y, EntityType type, String defaultAnimationId) {

        super(x, y, type, defaultAnimationId);

    }
}
