package fr.paragoumba.minimorpg.entities;

import fr.paragoumba.minimorpg.ResourceManager;
import fr.paragoumba.minimorpg.display.animations.Animation;
import fr.paragoumba.minimorpg.display.Frame;
import fr.paragoumba.minimorpg.display.animations.DynamicAnimation;
import fr.paragoumba.minimorpg.display.animations.StaticAnimation;
import fr.paragoumba.minimorpg.exceptions.AnimationNotFoundException;
import fr.paragoumba.minimorpg.utils.TextureName;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map.Entry;

public class EntityType {

    private EntityType(HashMap<String, BufferedImage> frames, HashMap<String, Animation> animations){

        this.frames = frames;
        this.animations = animations;

    }

    public static final String MARIO = "mario";
    public static final String SNOWMAN = "snowman";
    public static final String CHARIZARD = "charizard";

    private static HashMap<String, EntityType> entityTypes;

    private HashMap<String, BufferedImage> frames;
    private HashMap<String, Animation> animations;

    public static void init() {

        entityTypes = new HashMap<>();

        createEntityType(
                SNOWMAN,
                new DynamicAnimation(
                        "default",
                        new Frame(
                                "front", 1000
                        )
                ),
                new DynamicAnimation(
                        "walk",
                        new Frame(
                                "front", 500
                        ),
                        new Frame(
                                "right", 500
                        ),
                        new Frame(
                                "back", 500
                        ),
                        new Frame(
                                "left", 500
                        )

                )
        );

        createEntityType(
                MARIO,
                new StaticAnimation(
                        "front-stay",
                        new Frame(
                                "front-stay", 0
                        )
                ),
                new StaticAnimation(
                        "right-stay",
                        new Frame(
                                "right-stay", 0
                        )
                ),
                new StaticAnimation(
                        "back-stay",
                        new Frame(
                                "back-stay", 0
                        )
                ),
                new StaticAnimation(
                        "left-stay",
                        new Frame(
                                "left-stay", 0
                        )
                ),
                new DynamicAnimation(
                        "walk-front",
                        new Frame(
                                "front-walk-1", 300
                        ),
                        new Frame(
                                "front-stay", 300
                        ),
                        new Frame(
                                "front-walk-2", 300
                        ),
                        new Frame(
                                "front-stay", 300
                        )
                ),
                new DynamicAnimation(
                        "walk-right",
                        new Frame(
                                "right-walk-1", 300
                        ),
                        new Frame(
                                "right-stay", 300
                        ),
                        new Frame(
                                "right-walk-2", 300
                        ),
                        new Frame(
                                "right-stay", 300
                        )
                ),
                new DynamicAnimation(
                        "walk-back",
                        new Frame(
                                "back-walk-1", 300
                        ),
                        new Frame(
                                "back-stay", 300
                        ),
                        new Frame(
                                "back-walk-2", 300
                        ),
                        new Frame(
                                "back-stay", 300
                        )
                ),
                new DynamicAnimation(
                        "walk-left",
                        new Frame(
                                "left-walk-1", 300
                        ),
                        new Frame(
                                "left-stay", 300
                        ),
                        new Frame(
                                "left-walk-2", 300
                        ),
                        new Frame(
                                "left-stay", 300
                        )
                )
        );

        createEntityType(
                CHARIZARD,
                new DynamicAnimation(
                        "default",
                        new Frame(
                                "front", 1000
                        ),
                        new Frame(
                                "back", 1000
                        )
                )
        );
    }

    /**
     * Creates an EntityType using a HashMap containing its {@link Animation Animations} and an id.
     * @param entityId The id representing the entity. Will replace any other EntityType with the same id
     * @param animations A HashMap containing all the animations of the EntityType
     * @return Returns the created EntityType
     */
    public static EntityType createEntityType(String entityId, HashMap<String, Animation> animations){

        HashMap<String, BufferedImage> frames = new HashMap<>();

        for (Entry<String, Animation> animation : animations.entrySet()){
            for(String textureId : animation.getValue().getAllFramesIds()){

                String fullTextureName = new TextureName(entityId, textureId).getFullTextureName();

                frames.put(textureId, ResourceManager.getTexture("entities/" + fullTextureName));

            }
        }

        EntityType entityType = new EntityType(frames, animations);

        entityTypes.put(entityId, entityType);

        return entityType;

    }

    /**
     * Creates an EntityType using an array containing its {@link Animation Animations} and an id.
     * @param entityId The id representing the entity. Will replace any other EntityType with the same id
     * @param animations An array containing all the animations of the EntityType
     * @return Returns the created EntityType
     */
    public static EntityType createEntityType(String entityId, Animation... animations){

        HashMap<String, Animation> animationHashMap = new HashMap<>();

        for (Animation animation : animations){

            animationHashMap.put(animation.getId(), animation);

        }

        return createEntityType(entityId, animationHashMap);

    }

    /**
     * Returns the EntityType corresponding to the param entityId.
     * @param entityId The id of the EntityType to search for
     * @return The EntityType corresponding to the param entityId
     */
    public static EntityType getEntityType(String entityId){

        return entityTypes.get(entityId);

    }

    /**
     * Returns the {@link Animation} corresponding to the id.
     * @param animationId The id corresponding to the Animation to search for
     * @return The Corresponding Animation
     * @throws AnimationNotFoundException Thrown if the Animation can't be found (invalid id)
     */
    public Animation getAnimation(String animationId) throws AnimationNotFoundException {

        Animation animation = animations.get(animationId);

        if (animation == null){

            throw new AnimationNotFoundException(animationId);

        }

        return animation;

    }

    /**
     * Gets the actual frame of the {@link Animation}.
     * @param animation The Animation from which to get the actual frame
     * @return The frame to be displayed
     */
    public BufferedImage getActualFrame(Animation animation){

        return frames.get(animation.getActualFrameName());

    }
}
