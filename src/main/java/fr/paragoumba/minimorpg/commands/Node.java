package fr.paragoumba.minimorpg.commands;

import java.util.ArrayList;

public class Node {

    public Node(Node node){

        this(node.name, node.childNodes);

        action = node.action;

    }

    public Node(String name){

        this(name, new ArrayList<>());

    }

    public Node(String name, ArrayList<Node> childNodes){

        this.name = name;
        this.childNodes = childNodes;

    }

    private String name;
    private ArrayList<Node> childNodes;
    private CommandAction action;

    public String getName() {

        return name;

    }

    public Node followedBy(Node node){

        childNodes.add(node);

        return node;

    }

    public Node executes(CommandAction action){

        this.action = action;

        return new Node(this);

    }
}
