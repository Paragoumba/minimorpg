package fr.paragoumba.minimorpg.commands;

import java.util.ArrayList;

public class FloatNode extends Node {

    public FloatNode(String name) {

        super(name);

    }

    public FloatNode(String name, ArrayList<Node> childNodes) {

        super(name, childNodes);

    }
}
