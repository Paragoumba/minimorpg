package fr.paragoumba.minimorpg.commands;

import java.util.ArrayList;

public class StringNode extends Node {

    public StringNode(String name) {

        super(name);

    }

    public StringNode(String name, ArrayList<Node> childNodes) {

        super(name, childNodes);

    }
}
