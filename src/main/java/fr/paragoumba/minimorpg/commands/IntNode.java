package fr.paragoumba.minimorpg.commands;

import java.util.ArrayList;

public class IntNode extends Node {

    public IntNode(String name) {

        super(name);

    }

    public IntNode(String name, ArrayList<Node> childNodes) {

        super(name, childNodes);

    }
}
