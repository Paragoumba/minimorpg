package fr.paragoumba.minimorpg;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

public class ResourceManager {

    private static HashMap<String, BufferedImage> textures = new HashMap<>();
    private static final String resDirectory = "/res/";

    public static Font playerNameFont = new Font("Noto Sans Bold", Font.BOLD, 11);
    public static Font inventoryFont = new Font("Press Start 2P", Font.PLAIN, 19);

    public static Color brightShadow = new Color(255, 255, 255, 19);
    public static Color darkShadow = new Color(0, 0, 0, 105);

    public static final String NO_TEXTURE = "notexture";

    public static void init(){

        int width = 32;
        int height = 32;

        BufferedImage defaultTexture = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = defaultTexture.createGraphics();
        String text = "Error";

        g2d.clearRect(0, 0, width, height);
        g2d.setColor(Color.RED);
        g2d.drawString(
                text,
                (width - g2d.getFontMetrics().stringWidth(text)) / 2,
                (height + g2d.getFontMetrics().getAscent()) / 2);

        textures.put(NO_TEXTURE, defaultTexture);

        try{

            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(
                    Font.createFont(
                            Font.TRUETYPE_FONT,
                            ResourceManager.class.getResourceAsStream(resDirectory + "fonts/PressStart2P.ttf")
                    )
            );

        } catch (FontFormatException | IOException e) {

            e.printStackTrace();

        }
    }

    public static BufferedImage getTexture(String fullTextureName) {

        BufferedImage texture = textures.get(fullTextureName);

        if (texture == null){

            try{

                texture = ImageIO.read(
                        ResourceManager.class
                                .getResourceAsStream(resDirectory + "images/" + fullTextureName + ".png"));

            } catch (IllegalArgumentException | IOException e) {

                //e.printStackTrace();
                return getTexture(NO_TEXTURE);

            }

            textures.put(fullTextureName, texture);

        }

        return texture;

    }
}
